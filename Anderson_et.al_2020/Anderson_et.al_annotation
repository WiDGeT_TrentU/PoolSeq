#Tutorial can be found here for maker installation etc.: http://weatherby.genetics.utah.edu/MAKER/wiki/index.php/MAKER_Tutorial_for_WGS_Assembly_and_Annotation_Winter_School_2018#Add_functional_annotations_and_meta-data
#for downloading EST/ Protein evidence
#!/bin/bash
#SBATCH --time=3-00:00:00
#SBATCH --account=rrg-shaferab
#SBATCH --nodes=3
#SBATCH --ntasks-per-node=96
#SBATCH --mem-per-cpu
#SBATCH --job-name=maker_mg1
#SBATCH --output=%x-%j.out
srun -n 96  perl maker -R -q -base wtd_trial_2 -fix_nucleotides maker_opts.ctl maker_bopts.ctl maker_exe.ctl

#steps to make first training .hmm file for use with snap
mkdir snap
cd snap
perl /project/6007974/spencera/poolseq/genome/maker/bin/gff3_merge -d /project/6007974/spencera/poolseq/genome/maker/bin/wtd_ME.maker.output/wtd_ME_master_datastore_index.log
perl /project/6007974/spencera/poolseq/genome/maker/bin/maker2zff wtd_ME.all.gff
ls -1

/project/6007974/spencera/poolseq/genome/maker/exe/snap/fathom -categorize 1000 genome.ann genome.dna
/project/6007974/spencera/poolseq/genome/maker/exe/snap/fathom -export 1000 -plus uni.ann uni.dna
/project/6007974/spencera/poolseq/genome/maker/exe/snap/forge export.ann export.dna
perl /project/6007974/spencera/poolseq/genome/maker/exe/snap/hmm-assembler.pl wtd . > wtd1.hmm
"/project/6007974/spencera/poolseq/genome/maker/bin/snap/wtd1.hmm"

#Run .hmm file through maker with SNAP on and est2genome and protein2genome off
mkdir snap2
cd snap2
perl /project/6007974/spencera/poolseq/genome/maker/bin/gff3_merge -d /project/6007974/spencera/poolseq/genome/maker/bin/wtd_ME_SNAP1.maker.output/wtd_ME_SNAP1_master_datastore_index.log
perl /project/6007974/spencera/poolseq/genome/maker/bin/maker2zff wtd_ME_SNAP1.all.gff

/project/6007974/spencera/poolseq/genome/maker/exe/snap/fathom -categorize 1000 genome.ann genome.dna
/project/6007974/spencera/poolseq/genome/maker/exe/snap/fathom -export 1000 -plus uni.ann uni.dna
/project/6007974/spencera/poolseq/genome/maker/exe/snap/forge export.ann export.dna
perl /project/6007974/spencera/poolseq/genome/maker/exe/snap/hmm-assembler.pl wtd . > wtd2.hmm

"/project/6007974/spencera/poolseq/genome/maker/bin/snap2/wtd2.hmm"

#Run new .hmm again through SNAP
#Take results from that training session to create training data set for augustus; https://reslp.github.io/blog/My-MAKER-Pipeline/
mkdir augustus
cd augustus
perl /project/6007974/spencera/poolseq/genome/maker/bin/gff3_merge -d /project/6007974/spencera/poolseq/genome/maker/bin/wtd_ME_SNAP2.maker.output/wtd_ME_SNAP2_master_datastore_index.log
perl /project/6007974/spencera/poolseq/genome/maker/bin/maker2zff -x 0.01 wtd_ME_SNAP2.all.gff
#remove errors
/project/6007974/spencera/poolseq/genome/maker/exe/snap/fathom genome.ann genome.dna -validate > snap_validate_output.txt
cat snap_validate_output.txt | grep "error"
grep -vwE "MODEL6064" genome.ann > genome.ann2
#now that file is error free, finish input file
/project/6007974/spencera/poolseq/genome/maker/exe/snap/fathom -categorize 1000 genome.ann genome.dna
/project/6007974/spencera/poolseq/genome/maker/exe/snap/fathom -export 1000 -plus uni.ann uni.dna
/project/6007974/spencera/poolseq/genome/maker/exe/snap/forge export.ann export.dna

#Perl script which converts SNAP ZFF files to GBK files; script from https://github.com/hyphaltip/genome-scripts/blob/master/gene_prediction/zff2augustus_gbk.pl
module load bioperl/1.7.1
perl zff2augustus_gbk.pl > augustus.gbk

#split augustus.gbk file into a training and a test set
perl /project/6007974/spencera/poolseq/genome/maker/exe/Augustus/scripts/randomSplit.pl augustus.gbk 100

#create a new species for our AUGUSTUS training
export AUGUSTUS_CONFIG_PATH=/project/6007974/spencera/poolseq/genome/maker/exe/Augustus/config/
perl /project/6007974/spencera/poolseq/genome/maker/exe/Augustus/scripts/new_species.pl --species=WTD

# train AUGUSTUS with the training set file, evaluate the training and save the output of this test to a file for later reference
/cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/intel2018.3/augustus/3.3.2/bin/etraining --species=WTD augustus.gbk.train
/cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/intel2018.3/augustus/3.3.2/bin/augustus --species=WTD augustus.gbk.test | tee first_training.out

#improve prediction parameters of the models using the optimize_augustus.pl script - After 7 days this still did not complete
#!/bin/bash
#SBATCH --time=7-00:00:00
#SBATCH --account=rrg-shaferab
#SBATCH --nodes=1
#SBATCH --cpus=8
#SBATCH --mem=50G
#SBATCH --job-name=optimize_augustus
#SBATCH --output=%x-%j.out
module load bioperl/1.7.1
export AUGUSTUS_CONFIG_PATH=/project/6007974/spencera/poolseq/genome/maker/exe/Augustus/config/
perl /cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/intel2018.3/augustus/3.3.2/scripts/optimize_augustus.pl \
--species=WTD \
--kfold=16 \
--cpus=8 \
--aug_exec_dir=/cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/intel2018.3/augustus/3.3.2/bin/ \
augustus.gbk.train

#retrain and test AUGUSTUS with the optimized paramters and compare the results to the first run
/cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/intel2018.3/augustus/3.3.2/bin/etraining --species=WTD augustus.gbk.train
/cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/intel2018.3/augustus/3.3.2/bin/augustus --species=WTD augustus.gbk.test | tee second_training.out


#extract the final annotation
perl /project/6007974/spencera/poolseq/genome/maker/bin/gff3_merge -n -d /project/6007974/spencera/poolseq/genome/maker/bin/wtd_ME_AUGUSTUS.maker.output/wtd_ME_AUGUSTUS_master_datastore_index.log
#Only genes
ls -l | awk '$3 == "gene"' wtd_ME_AUGUSTUS.all.gff > wtd_ME_AUGUSTUS_genes.all.gff




#Trying to figure out how to identify stop codons within CDS:https://www.ebi.ac.uk/Tools/st/emboss_transeq/
ls -l | awk '$3 == "CDS"' wtd_ME_AUGUSTUS.all.gff > wtd_ME_AUGUSTUS_CDS.all.gff
module load bedtools
bedtools getfasta -fi /project/6007974/spencera/poolseq/genome/maker/data/wtd_genome_masked.fasta -bed wtd_ME_AUGUSTUS_CDS.all.gff > wtd_annotation_cds.fasta
#translates nucleotide to protein
module load EMBOSS
transeq -sequence wtd_annotation_cds.fasta
grep -c "^>" wtd_annotation_cds.fasta
#To check how many lines don't prematurly have stop codons
sed -e 's/\(^>.*$\)/#\1#/' wtd_annotation_cds_transeq.fasta | tr -d "\r" | tr -d "\n" | sed -e 's/$/#/' | tr "#" "\n" | sed -e '/^$/d' | grep -v ^">" | grep -v "*" | wc -l
#number of CDS
148958
#number without premature stop codons
84223



#Extract fasta and gff from maker output
mkdir wtd_annotation_ME
perl /project/6007974/spencera/poolseq/genome/maker/bin/fasta_merge -d /project/6007974/spencera/poolseq/genome/maker/bin/wtd_ME_AUGUSTUS.maker.output/wtd_ME_AUGUSTUS_master_datastore_index.log
perl /project/6007974/spencera/poolseq/genome/maker/bin/gff3_merge -n -d /project/6007974/spencera/poolseq/genome/maker/bin/wtd_ME_AUGUSTUS.maker.output/wtd_ME_AUGUSTUS_master_datastore_index.log

#need to split the fasta file into multiple = actually the problem is a single fasta line I believe! (maker-ref0002725-augustus-gene-5.0-mRNA-1 protein AED:0.21 eAED:0.21 QI:0|0.75|0.76|0.94|0.43|0.58|17|1228|1115)
module load kentutils/20180716
faSplit about wtd_ME_AUGUSTUS.all.maker.proteins.fasta 1100000 protein_fasta/seq

#this finally works!!!
#!/bin/bash
#SBATCH --time=01:00:00
#SBATCH --account=rrg-shaferab
#SBATCH --cpus=2
#SBATCH --mem=8G
#SBATCH --job-name=blastp
#SBATCH --output=%x-%j.out
module load gcc/7.3.0
module load blast+/2.9.0
blastp -query wtd_annotation_ME/trial.protein.fasta -db blastdb/nr -taxids 9606 -max_hsps 1 -max_target_seqs 1 -outfmt 6 -out wtd_annotation_ME/trial.protein.blastp


#!/bin/bash
#SBATCH --time=03:00:00
#SBATCH --account=rrg-shaferab
#SBATCH --cpus=1
#SBATCH --mem=4G
#SBATCH --job-name=blastp_multi
#SBATCH --output=%x-%j.out
module load gcc/7.3.0
module load blast+/2.10.0
echo ${1}
blastp -query ${1}.fa -db blastdb/nr -taxids 9606 -max_hsps 1 -max_target_seqs 1 -outfmt 6 -out ${1}.ref
#END

#Loop in directory
for f in $(ls wtd_annotation_ME/protein_fasta/*.fa | cut -f1 -d'.')
do echo ${f}
sbatch -o ${f}-%A.out blastp_multi.sh ${f}
sleep 10
done


#this works, but can't submit job (graham doesn't connect to internet) - but cedar does
blastp -query ${1}.fasta -remote -db nr -entrez_query "Homo sapiens [Organism]" -outfmt '6 qseqid sseqid sacc stitle pident evalue' -num_alignments 1 -out ${1}.blastp

for f in $(ls *.fa | cut -f1 -d'.')
do echo ${f}
sbatch -o ${f}-%A.out blastp_remote.sh ${f}
sleep 10
done





#interproscan on cluster
module load interproscan
interproscan.sh -appl pfam -dp -f TSV -goterms -iprlookup -pa -t p -i wtd_ME_AUGUSTUS.all.maker.proteins.fasta -o wtd_ME_AUGUSTUS.all.maker.proteins.fasta.iprscan

#Rename gene models and other data.

perl maker_map_ids --prefix WTD_ --justify 8 wtd_annotation_ME/wtd_ME_AUGUSTUS.all.gff > wtd_annotation_ME/map
perl map_gff_ids wtd_annotation_ME/map wtd_annotation_ME/wtd_ME_AUGUSTUS.all.gff
perl map_fasta_ids wtd_annotation_ME/map wtd_annotation_ME/wtd_ME_AUGUSTUS.all.maker.proteins.fasta
perl map_fasta_ids wtd_annotation_ME/map wtd_annotation_ME/wtd_ME_AUGUSTUS.all.maker.transcripts.fasta
perl map_data_ids wtd_annotation_ME/map wtd_annotation_ME/wtd_ME_AUGUSTUS.all.maker.proteins.fasta.nr.human.blastp
perl map_data_ids wtd_annotation_ME/map wtd_annotation_ME/wtd_ME_AUGUSTUS.all.maker.proteins.fasta.iprscan

#Integrate functional annotations into structural annotations.

perl ipr_update_gff wtd_annotation_ME/wtd_ME_AUGUSTUS.all.gff wtd_annotation_ME/wtd_ME_AUGUSTUS.all.maker.proteins.fasta.iprscan > wtd_annotation_ME/wtd_ME_AUGUSTUS2_2.all.gff
perl iprscan2gff3 wtd_annotation_ME/wtd_ME_AUGUSTUS.all.maker.proteins.fasta.iprscan wtd_annotation_ME/wtd_ME_AUGUSTUS.all.gff > wtd_annotation_ME/wtd_ME_AUGUSTUS2_2.all.gff

perl maker_functional_gff uniprot_sprot.fasta wtd_annotation_ME/wtd_ME_AUGUSTUS.all.maker.proteins.fasta.nr.human.blastp wtd_annotation_ME/wtd_ME_AUGUSTUS2_2.all.gff > wtd_annotation_ME/wtd_ME_AUGUSTUS2_3.all.gff
perl maker_functional_fasta uniprot_sprot.fasta wtd_annotation_ME/wtd_ME_AUGUSTUS.all.maker.proteins.fasta.nr.human.blastp wtd_annotation_ME/wtd_ME_AUGUSTUS.all.maker.proteins.fasta > wtd_annotation_ME/wtd_ME_AUGUSTUS2.all.maker.2.proteins.fasta
perl maker_functional_fasta uniprot_sprot.fasta wtd_annotation_ME/wtd_ME_AUGUSTUS.all.maker.proteins.fasta..nr.human.blastp wtd_annotation_ME/wtd_ME_AUGUSTUS.all.maker.transcripts.fasta > wtd_annotation_ME/wtd_ME_AUGUSTUS2.all.maker.2.transcripts.fasta
